const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");



//No. 4 Retrieve all  products
router.get('/all', (req, res) => {
	productController.retrieveAllActiveProducts(req.body.isActive).then(result => res.send(result));
})

// Retrieve all active products
router.get('/', (req, res) => {
    productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

//No. 5 Retrieve Single Product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(result => res.send(result));
})




//No. 6 Create Product (Admin Only)
router.post('/', auth.verify, (req, res) =>{

	const userData = auth.decode(req.headers.authorization)

	productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result => res.send(result))
})

//No. 7 Update Product information (Admin only)
router.post('/:productId', auth.verify, (req, res) => {
    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
})




//No. 8 Archive Product (Admin Only)
router.put('/:productId/archive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.archiveProduct(req.body, req.params, userData)
    .then(result => res.send(result))
})



module.exports = router