const express = require('express');
const router = express.Router();
const userController = require('../controllers/user')
const auth = require("../auth");


//No. 1 Routes for User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body)
})


//No. 2 Routes for authenticating a user
//Routes for authenticating a user
router.post('/login', (req, res) => {

	const email = req.body.email;
	const password = req.body.password;


	userController.loginUser(req.body).then(result => res.send(result))
})

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route for checkout
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}
	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router