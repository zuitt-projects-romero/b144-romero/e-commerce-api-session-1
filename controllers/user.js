const User = require('../models/User');
const Order = require('../models/Order');
const Product = require('../models/Product');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
// No. 1 User Registration
module.exports.userRegistration = (reqBody, callback) => {


	let newUser = new User({

		email: reqBody.email,
		password: reqBody.password
	})
	return bcrypt.genSalt(10, function(err, salt){
		bcrypt.hash(newUser.password, salt, function(err, hash){
			if(err){
				return false
			} 
			newUser.password = hash;
			newUser.save()
			return true
		})
		
	})
}


//No. 2 User authentication
/*
Steps:
1. Check if the user email exist in our database. If user does not exist, return false
2. if the user exists, Compare the password provided in the login form with the password stored in the database.
3. Generate/return a jsonwebtoken if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result.toObject())}
			}else{
				return false
			}
		}
	})
}

// Get all Users
module.exports.getAllUser = () => {

	return User.find({}).then(result => {

		return result

	});

};


// No. 3 Set user as Admin (Admin Only)
module.exports.setAsAdmin = (reqParams, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin) {

            let updatedUser = {
              	isAdmin: true
            }
        
            return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
                if (error) {
                	return false
                }
                else{
                	return "User is now an Admin"
                }
            })

        } else {
            return "You are not an Admin"
        }
        
    });    
}






// Check email

module.exports.checkEmailExists = (reqBody) =>{
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		} else{
			//no duplicate email found
			return false;
		}
	})
}




// User details

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};



// Checkout

module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders = data.orders;

		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	let newOrder = new Order({
		userId: data.userId,
		products: data.orders,
		total: data.total
	})
	return await newOrder.save().then((success, err) =>{
		if(err){
			return false;
		}
		else{
			return true;
		}
	})
}