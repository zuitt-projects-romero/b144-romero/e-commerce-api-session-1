const Product = require('../models/Product');

//No. 4 Retrieve All Active Products
module.exports.retrieveAllActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
};
//No. 5 Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

//No. 6 Create Product (Admin Only)

module.exports.addProduct = (reqBody, userData) => {
    let newProduct = new Product({
        Category: reqBody.category,
        Name: reqBody.name,
        Price: reqBody.price,
        Description: reqBody.description,
        Image: reqBody.image
        Stocks: reqBody.stocks,
        isActive: reqBody.isActive
    });
    return newProduct.save().then((product, err) => {
        if(err){
            return false
        } else {
            return true
        }
    })
}

//No. 7  Update a Product (Admin Only)
module.exports.updateProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        _id: reqBody.Id,
        Name: reqBody.Name,
        Price: reqBody.Price,
        Description: reqBody.Description,
        Image: reqBody.Image,
        Stocks: reqBody.Stocks,
        isActive: reqParams.isActive
    }
    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, err) => {
        if(err){
            return false
        } else {
            return true
        }
    })
}





// No. 8 Archive product (Admin Only)
module.exports.archiveProduct = (reqbody, reqParams, userData) => {
	return User.findById(userData.userId).then(result => {
	if(userData.isAdmin){	

		let updatedProduct = {
		isActive : false
	}

	return Product.findByIdAndUpdate(reqParams.courseId, updatedProduct).then((product, error) => {

		if (error){
			return "Archive failed"
		}
		else{
			return "Product archived successfully";
		}
	})

	}else{
		return "You are not an admin"
	}
 	})
}