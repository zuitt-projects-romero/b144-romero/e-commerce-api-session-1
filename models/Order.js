const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({

  createdOn: {
    type: Date,
    default: new Date()
  },
  userId: {
    type: String,
    required: [true, 'UserId is required']
  },
  products: String,
  status: {
    type: String,
    default: 'Pending'
  },
  total: {
    type: Number
  }
})


module.exports = mongoose.model('Order', orderSchema)
